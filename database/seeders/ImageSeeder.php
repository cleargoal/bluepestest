<?php

namespace Database\Seeders;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 7; $i++) {
            $newImage = new Image();
            $newImage->image = "image#$i.png";
            $newImage->name = "Amazing image numbered #$i";
            $newImage->save();
        }
    }
}
