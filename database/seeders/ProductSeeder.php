<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = ['computer', 'tablet', 'smartphone', 'smart-watch', 'notebook', 'monitor', 'system-block',];
        foreach ($products as $product) {
            $newProd = new Product();
            $newProd->name = $product;
            $newProd->description = "It's a just $product";
            $newProd->save();
        }
    }
}
