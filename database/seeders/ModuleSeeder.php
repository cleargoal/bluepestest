<?php

namespace Database\Seeders;

use App\Models\Module;
use App\Models\Order;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Order::all()->count();
        for ($o = 0; $o < 12; $o++) {
            $module = new Module;
            $module->order_id = rand(1, $orders);
            $module->module = Arr::random(['module1', 'module2']);
            $module->custom_field_1 = Str::random();
            $module->custom_field_2 = Str::random();
            $module->save();
        }
    }
}
