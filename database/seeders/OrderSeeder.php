<?php

namespace Database\Seeders;

use App\Models\Order;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i < 10; $i++) {
            $order = new Order();
            $order->email = Arr::random(['first', 'second', 'third', 'fourth', 'fifth', 'sixth']) . '@domen_name.' . Arr::random(['com', 'net', 'org', 'info']);
            $order->phone = Arr::random(['067', '068', '069',]) . '-' . rand(111, 999) . '-' . rand(1111, 9999);
            $order->save();
        }
    }
}
