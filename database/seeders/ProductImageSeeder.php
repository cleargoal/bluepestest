<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();
        $images = Image::all();
        $imagesCount = $images->count();
        foreach ($products as $product) {
            $imgRand = rand(0, $imagesCount);
            $idUsed = [];
            for ($i = 0; $i < $imgRand; $i++) {
                $imageId = $imgRand;
                while (in_array($imageId, $idUsed)) {
                    $imageId = rand(1, $imgRand);
                }
                $idUsed[] = $imageId;
                DB::table('product_images')->insert([
                    'product_id' => $product->id,
                    'image_id' => $imageId,
                ]);
            }
        }
    }
}
