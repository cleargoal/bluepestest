<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Vendor;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VendorProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vendors = Vendor::all();
        $products = Product::all();
        foreach($vendors as $vendor) {
            foreach($products as $product) {
                DB::table('vendor_products')->insert([
                    'vendor_id' => $vendor->id,
                    'product_id' => $product->id,
                    'price' => rand(1100, 10000)/100,
                    'quantity' => rand(10, 500),
                ]);
            }
        }
    }
}
