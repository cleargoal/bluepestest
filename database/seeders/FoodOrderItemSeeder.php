<?php

namespace Database\Seeders;

use App\Models\Food;
use App\Models\FoodOrder;
use App\Models\FoodOrderItem;
use Illuminate\Database\Seeder;

class FoodOrderItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orderIds = FoodOrder::pluck('id');
        $foodAmount = Food::all()->count();
        foreach ($orderIds as $order) {
            $itemsAmount = rand(1, 20);
            $itemsIds = [];
            for ($i = 0; $i < $itemsAmount; $i++) {
                $item = new FoodOrderItem();
                $item->order_id = $order;
                $foodId = 0;
                while ($foodId === 0 || in_array($foodId, $itemsIds)) {
                    $foodId = rand(1, $foodAmount);
                }
                $item->food_id = $foodId;
                $itemsIds[] = $foodId;
                $item->quantity = rand(1, 8);
                $item->save();
            }
        }
    }
}
