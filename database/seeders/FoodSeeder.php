<?php

namespace Database\Seeders;

use App\Models\Food;
use Illuminate\Database\Seeder;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $foods = ['Meat', 'Milk', 'Bread', 'Cheese', 'Apple', 'Banana', 'Lemon', 'Cabbage', 'Tomato',];
        $props = ['fresh', 'salt', 'frozen', 'grilled', 'boiled', 'marinated'];

        foreach($foods as $food) {
            foreach ($props as $prop) {
                $newFood = new Food();
                $newFood->product = $food . ' ' . $prop;
                $newFood->price = rand(1, 5000);
                $newFood->save();
}        }
    }
}
