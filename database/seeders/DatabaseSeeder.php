<?php

namespace Database\Seeders;

use App\Models\Food;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            VendorSeeder::class,
            ProductSeeder::class,
            ImageSeeder::class,
            VendorProductSeeder::class,
            ProductImageSeeder::class,
            OrderSeeder::class,
            ModuleSeeder::class,
            FoodSeeder::class,
            FoodOrderSeeder::class,
            FoodOrderItemSeeder::class,
        ]);
    }
}
