<?php

namespace Database\Seeders;

use App\Models\FoodOrder;
use Illuminate\Database\Seeder;

class FoodOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 25; $i++) {
            $order = new FoodOrder();
            $order->order_name = 'Food Order #0000' . $i;
            $order->save();
        }
    }
}
