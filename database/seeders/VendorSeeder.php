<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\Models\Vendor;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newVendor = new Vendor();
        $newVendor->title = 'Apple';
        $newVendor->description = 'Apple Computers Inc.';
        $newVendor->save();

        $newVendor = new Vendor();
        $newVendor->title = 'Samsung';
        $newVendor->description = 'Samsung Computers Inc.';
        $newVendor->save();
    }
}
