<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use \Illuminate\Http\JsonResponse;

class VendorProductController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json(Vendor::with('products.images')->get());
    }

    public function imagine()
    {
        return Vendor::with(['products' =>
            function ($query) {
                $query->withCount('images')
                    ->having('images_count', '>', 1)
                    ->with('images');
            }
        ])
            ->get();
    }
}
