<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UniqueArrayMembersRequest;
use \Illuminate\Http\JsonResponse;

class TransportationController extends Controller
{
    public function trans(Request $request)
    {
        $vehicles = floatval(str_replace(',', '.', $request->params[0]));
        $hours = floatval(str_replace(',', '.', $request->params[1]));
        return (1 / 1.5) * $vehicles * $hours;
    }

    public function unique(Request $request): JsonResponse
    {
        $response = ['result' => 'false', 'message' => 'incorrect Input'];
        if (!empty($request->params) and !ctype_digit($request->params)) {
            $inputStr = str_replace([' ', '.', ';',], ',', $request->params);
            $inputArr = explode(',', $inputStr);
            if (count($inputArr) > 2) {
                $arrayUnique = array_unique($inputArr);
                if (count($arrayUnique) === 2) {
                    $unique = null;
                    foreach ($arrayUnique as $uniq) {
                        $found = 0;
                        $uni = $uniq;
                        foreach($inputArr as $input) {
                            if ($uniq === $input) {
                                $found++;
                            }
                            if ($found > 1) {
                                break;
                            }
                        }
                        if ($found === 1) {
                            $unique = $uni;
                            break;
                        }
                    }

                    $response = $unique !== null ? ['result' => 'true', 'message' => $unique] : ['result' => 'false', 'message' => 'incorrect Input'];
                }
            }
        }
        return response()->json($response);
    }
}
