<?php

namespace App\Http\Controllers;

use App\Models\FoodOrder;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function module(Request $request)
    {
        $query = Order::with('modules');
        $requestModule = $request->module;
        if ($request->has('module') && $request->input('module') !== 'null') {
            $query = Order::whereHas('modules', function ($query) use ($requestModule) {
                $query->where('module', $requestModule);
            })
                ->with(['modules' => function ($query) use ($requestModule) {
                    $query->where('module', $requestModule);
                }]);
        }
        return $query->get();
    }

    public function total()
    {
        $foodOrders = FoodOrder::with('food')->get();

        $foodOrders->map(function ($order) {
            return $order->food->transform(function ($food, $order) {
                $food->setTempQuantity($food->pivot->quantity);
                $food->append('itemTotal');
                return $food;
            });
        });

        return $foodOrders;
    }
}
