<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Food extends Model
{
    use HasFactory;
    protected $fillable =  ['itemTotal'];
    protected $attributes = ['itemTotal'];
    protected $quantity;

    public function foodOrders(): BelongsToMany
    {
        return $this->belongsToMany(FoodOrder::class, 'food_order_items', 'food_id');
    }

    public function getItemTotalAttribute()
    {
        return $this->price * $this->quantity/100;
    }

    public function setTempQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

}
