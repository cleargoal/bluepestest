<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory;

    public function vendors() :BelongsToMany
    {
        return $this->belongsToMany(Vendor::class, 'vendor_products')->withPivot('price', 'quantity');
    }

    public function images() :BelongsToMany
    {
        return $this->belongsToMany(Image::class, 'product_images');
    }

    public function countImages(): int
    {
        return $this->belongsToMany(Product::class, 'product_images')->count();
    }

}
