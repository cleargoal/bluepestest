<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\HasMany;
use \Illuminate\Database\Eloquent\Relations\BelongsToMany;

class FoodOrder extends Model
{
    use HasFactory;

    public function foodOrderItems(): HasMany
    {
        return $this->hasMany(FoodOrderItem::class, 'order_id');
    }

    public function food(): BelongsToMany
    {
        return $this->belongsToMany(Food::class, 'food_order_items', 'order_id')->withPivot('quantity');
    }
}
