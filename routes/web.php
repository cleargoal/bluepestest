<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/all-products', 'App\Http\Controllers\VendorProductController@index')->name('all.vendors.products');
Route::post('/products-many-images', 'App\Http\Controllers\VendorProductController@imagine')->name('vendor.products.many.images');

Route::post('/order-module', 'App\Http\Controllers\OrderController@module')->name('order.modules');
Route::post('/orders_total', 'App\Http\Controllers\OrderController@total')->name('orders.total');

Route::post('/trans', 'App\Http\Controllers\TransportationController@trans')->name('trans.calc');

Route::post('/unique', 'App\Http\Controllers\TransportationController@unique')->name('find.unique');
